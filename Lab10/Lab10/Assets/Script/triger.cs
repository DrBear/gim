﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class triger : MonoBehaviour
{

    public GameObject Door;
    public Animator Anim;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        Anim = Door.GetComponent<Animator>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == ("Player"))
        {
            Anim.SetTrigger("Open");
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == ("Player"))
        {
            Anim.SetTrigger("Close");
        }
    }
}
